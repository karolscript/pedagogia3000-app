import React from "react"

import "./Home.scss"
import Homecarousel from '../home/home-carousel/Homecarousel';
import Homegrid from "./home-grid/Homegrid";

const Home = () => {
    return (
        <>
        <Homecarousel></Homecarousel>
        <Homegrid></Homegrid>
        </>
    )
}

export default Home