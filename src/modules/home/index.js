import React from 'react';

const Home = () => (
    <div>Home Module</div>
);

export default {
    routeProps: {
        path: '/',
        exact: true,
        component: Home,
    },
    name: 'Home',
};