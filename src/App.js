import React from 'react'
import AppNav from './shared-components/top-header/navbar/Nav'
import TopHeader from './shared-components/top-header/Topheader.js'
import Home from './modules/home/Home.js';
import Footer from './shared-components/footer/Footer';
import { BrowserRouter, Route } from 'react-router-dom'
import Posts from './modules/posts/Posts';

function App() {
    return (
        <>
            <div className="content-body">
                <BrowserRouter>
                    <TopHeader/>
                    <AppNav/>
                    <Route exact path="/" component={Home} />
                    <Route path="/posts" component={Posts} />
                </BrowserRouter>
            </div>
            <Footer></Footer>
        </>
    )
}

export default App