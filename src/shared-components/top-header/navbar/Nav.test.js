import { render, cleanup, screen  } from '@testing-library/react'; 
import AppNav from './Nav'

// include as many test cases as you want here
const links = [
    { text: 'Contacto', location: "/contact" },
    { text: '¿Quiénes Somos?', location: "/about" },
    { text: 'Artículos', location: "/blog" },
    { text: 'Red Mundial', location: "/world" },
  ];


// I use test.each to iterate the test cases above
test.each(links)(
    "Check if Nav Bar have %s link.",
    (link) => {
      render(<AppNav />);
  //Ensure the text is in the dom, will throw error it can't find
      const linkDom = screen.getByText(link.text); 
  
  //use jest assertion to verify the link property
      expect(linkDom).toHaveAttribute("href", link.location);
    }
  );

  test('Check if have logo and link to home page', () => {
  render(<AppNav/>);
        // get by TestId define in the navBar
    const logoDom = screen.getByTestId(/Logo/); 
        // check the link location
    expect(logoDom).toHaveAttribute("href", "/"); 
    //check the logo image
  expect(screen.getByAltText(/Logo/)).toBeInTheDocument(); 
});