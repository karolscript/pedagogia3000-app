import React from 'react';

const Posts = () => (
    <div>Posts Module</div>
);

export default {
    routeProps: {
        path: '/posts',
        exact: true,
        component: Posts,
    },
    name: 'Posts',
};