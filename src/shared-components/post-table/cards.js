import React from 'react'
import Card from './card.js';

const cardsObject = [
    {
        id: 1,
        title: 'Sample Text'
    },
    {
        id: 2,
        title: 'Sample Testing'
    },
    {
        id: 3,
        title: 'Sample Pracice'
    },
]

function Cards() {
    return (
        <div className="container d-flex justify-content-center align-items-center h-100">
            <div className="row">
                {
                    cardsObject.map(card =>(
                        <div className="col-md-4" key={card.id}>
                            <Card/>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Cards
