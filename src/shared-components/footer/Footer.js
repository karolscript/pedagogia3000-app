import React from "react";
import './Footer.scss';
import { Container, Row, Nav, Navbar, Col, Image} from 'react-bootstrap'; 

const Footer = () => {
  return (
    <>
    <Container fluid className="footer-container">
        <Row className="footer-center">
            <p>Aquí va un contacto, pueden ser teléfonos, dirección etc.</p>
        </Row>
        <Row className="footer-navbar">
            <Nav className="justify-content-center" activeKey="/home">
                <Nav.Item>
                <Nav.Link className="footer-navbar-link" href="/home">Home</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                <Nav.Link className="footer-navbar-link" eventKey="link-1">Red Mundial</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                <Nav.Link className="footer-navbar-link" eventKey="link-2">¿Quińes Somos?</Nav.Link>
                </Nav.Item>                
                <Nav.Item>
                <Nav.Link className="footer-navbar-link" eventKey="link-2">Artículos</Nav.Link>
                </Nav.Item>
            </Nav>
        </Row>
        <Row className="footer-center">
            © 2020 Fundación Pedagooogía 3000 Venezuela.
        </Row>
    </Container>


    </>
  )};
  export default Footer;