import React from "react";
import './Nav.scss';
import {NavLink, withRouter}  from 'react-router-dom'

import { Nav, Navbar, Image } from 'react-bootstrap';

import Burguer from '../../../assets/icons/burguer.png';

const AppNav = (props) => {

const getNavLinkClass = (path) => {
    return props.path === path ? 'active' : '';
  }


  return (
    <>
    <Navbar className="principal-navbar" collapseOnSelect expand="md" sticky="top" navbar="false"> 
    <Navbar.Toggle className="navbar-toggle" aria-controls="basic-navbar-nav">
      <Image className="toggle-button" src={Burguer}/>
      </Navbar.Toggle>
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav fill className="justify-content-center mx-auto nav-mx" activeKey="/home">
      <Nav.Item className="navbar-item">
        <NavLink  className="navbar-link" to="/" >Home</NavLink>

        {/*<Nav.Link className="navbar-link" href="/home">Home</Nav.Link>*/}
      </Nav.Item >
      <Nav.Item className="navbar-item">
        <Nav.Link className="navbar-link" eventKey="/world">Red Mundial</Nav.Link>
      </Nav.Item>
      <Nav.Item className="navbar-item">
        <Nav.Link className="navbar-link" eventKey="/about">¿Quiénes Somos?</Nav.Link>
      </Nav.Item>
      <Nav.Item className="navbar-item">
       {/*} <Nav.Link className="navbar-link" eventKey="/posts">Artículos</Nav.Link>*/}
       <NavLink  className="navbar-link" to="/posts" >Artículos</NavLink>
      </Nav.Item>
      <Nav.Item className="navbar-item">
        <Nav.Link className="navbar-link" eventKey="/contact">Contacto</Nav.Link>
      </Nav.Item>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
  </>

  )};
  export default AppNav;