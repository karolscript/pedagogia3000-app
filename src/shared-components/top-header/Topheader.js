import React from "react";
import { Container, Row, Col, Image, Button, ButtonGroup } from 'react-bootstrap'; 
import './Topheader.scss';
import  Boy  from '../../assets/images/boy.webp';
import  Girl  from '../../assets/images/girl.webp';
import twitter from '../../assets/social-icons/twitter-bird.png';
import facebook from '../../assets/social-icons/facebook.png';
import instagram from '../../assets/social-icons/instagram.png';
import Logo from '../../assets/images/LOGO_PE3000v2018.jpg';

const TopHeader = () => (

    <Container fluid>
    <Row className="top-header-row">
    <Col xs={12} lg={2} className="social">
            <ButtonGroup size="sm">
                <Button className="btn-social" variant="warning"><Image className="social-icon" src={twitter} fluid/></Button>
                <Button className="btn-social" variant="warning"><Image className="social-icon" src={facebook} fluid/></Button>
                <Button className="btn-social" variant="warning"><Image className="social-icon" src={instagram} fluid/></Button>
            </ButtonGroup>
        </Col>
  
        <Col xs={12} lg={10} className="col-title"><Image className="" src={Logo} fluid/><h3 className="second-title">¡Una educación diferente es posible!</h3></Col>
        
    </Row>
    </Container>

  );
  export default TopHeader;