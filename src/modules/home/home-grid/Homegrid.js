import React from "react";
import { Container, Row, Col, Image} from 'react-bootstrap'; 
import './Homegrid.scss';
import  BoyPlay  from '../../../assets/images/home-grid/boy-play.jpeg';
import  KidsRun  from '../../../assets/images/home-grid/kids-run.jpeg';
import  GirlEat  from '../../../assets/images/home-grid/girl-eat.jpeg';
import  GirlFlag  from '../../../assets/images/home-grid/girl-flag.jpeg';
import  GirlHands  from '../../../assets/images/home-grid/girl-hands.jpg';
import  KidsWorld  from '../../../assets/images/home-grid/kids-world.jpg';
import  KidsSchool  from '../../../assets/images/home-grid/kids-school.jpeg';
import  KidsStory  from '../../../assets/images/home-grid/kids-story.jpeg';
import  KidsTeacher  from '../../../assets/images/home-grid/kids-teacher.jpeg';
import  KidsEat  from '../../../assets/images/home-grid/kids-eat.jpeg';

const Homegrid = () => (

    <Container className="no-padding" fluid>
        <Row className="home-grid-row">
            <Col xs={12} md={4}><h2 className="grid-title">ESTE ES UN TÍTULO</h2></Col>
            <Col xs={6}  md={2} className="image-col"><div><Image className="image" src={GirlFlag} fluid/></div></Col>
            <Col xs={6}  md={2} className="image-col"><div><Image className="image" src={KidsRun}  fluid/></div></Col>
            <Col xs={6}  md={2} className="image-col"><div><Image className="image" src={GirlEat}  fluid/></div></Col>
            <Col xs={6}  md={2} className="image-col"><div><Image className="image" src={BoyPlay}  fluid/></div></Col>
        </Row>
        <Row className="home-grid-row">
            <Col xs={6} md={2}><div><Image className="image" src={KidsWorld} fluid/></div></Col>
            <Col xs={6} md={2}><div><Image className="image" src={GirlHands} fluid/></div></Col>
            <Col xs={6} md={2} className="image-col" ><div><Image className="image" src={KidsSchool}  fluid/></div></Col>
            <Col xs={6} md={2} className="image-col" ><div><Image className="image" src={KidsStory}   fluid/></div></Col>
            <Col xs={6} md={2} className="image-col" ><div><Image className="image" src={KidsTeacher} fluid/></div></Col>
            <Col xs={6} md={2} className="image-col" ><div><Image className="image" src={KidsEat}     fluid/></div></Col>
        </Row>
    </Container>

  );
  export default Homegrid;