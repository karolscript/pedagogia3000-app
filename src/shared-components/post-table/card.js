import React from 'react'

function card() {
    return (
        <div className="card">
            <img src="" alt=""/>
            <div className="card-body">
                <h4 className="card-title">Post</h4>
                <p className="card-text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid, similique ratione neque rerum atque rem reprehenderit animi repudiandae et doloribus non eveniet ut ducimus! At reiciendis atque veniam autem debitis!</p>
                <p className="card-text text-secondary">17 de mayo</p>
            <a href="#!" className="btn btn-outline-secondary rounded-0">
                Ver Más...
            </a>
            </div>
        </div>
    )
}

export default card
